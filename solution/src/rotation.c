#include "../include/util.h"

static struct pixel pixel_get(struct image *img, size_t x, size_t y) {
    struct pixel result;
    result = img->data[y + x * img->width];
    return result;
}

static void pixel_set(struct image *img, struct pixel pixel, size_t x, size_t y)
{
   img->data[y + x * img->width] = pixel;
}

struct image rotate(struct image *image) {
    size_t w = image->width;
    size_t h = image->height;
    struct image rotated_image = image_set(w, h);
    for (size_t i = 0; i < h; ++i) {
        for (size_t j = 0; j < w; ++j) {
            struct pixel pixel = pixel_get(image, i, j);
            pixel_set(&rotated_image, pixel, j, rotated_image.width - i - 1);
        }
    }
    return rotated_image;
}
