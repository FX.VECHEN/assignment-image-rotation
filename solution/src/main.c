#define _CRT_SECURE_NO_WARNINGS
#include "util.h"

int main(int argc, char *argv[]) {
    
    if (argc !=3) {
        return -1;
    }

    const char* const f_in = argv[1];
    const char* const f_out = argv[2];

    struct image im_old;
    struct image im_new;

    FILE *in = fopen(f_in, "rb");

    if (!in) 
        return -2;
    

    // Считать заголовок + картинку
    if (!from_bmp(in, &im_old))
        return -3;

    // Повернуть картинку
    im_new = rotate(&im_old);

    // Открыть новый файл
    FILE *out = fopen(f_out, "wb");
    if (!out)
        return -4;
    


    // Записать повернутую картинку в новый файл
    if (!to_bmp(out, &im_new))
        return -5;

    fclose(in);
    fclose(out);

    image_deinit(&im_old);
    image_deinit(&im_new);

    return 0;
}
