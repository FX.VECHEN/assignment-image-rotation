#include "../include/image.h"
#include "../include/util.h"

struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));

struct image image_set(size_t height, size_t width)
{
    return (struct image) {.height = height, .width = width, .data = malloc(height * width * sizeof(struct pixel))};
}

static uint8_t padding_calculate(struct image const *img)
{
    return (4 - (img->width * 3 % 4)) % 4;
}

void image_deinit(struct image *img)
{
    free(img->data);
}

bool from_bmp(FILE *in, struct image *img)
{
    struct bmp_header bmp_header = {0};
    if (!fread(&bmp_header, 1, sizeof(struct bmp_header), in))
        return false;
    *img = image_set(bmp_header.biHeight, bmp_header.biWidth);
    uint64_t i = 0;
    uint8_t const padding = padding_calculate(img);
    for (uint64_t y = 0; y < img->height; ++y)
    {
        for (uint64_t x = 0; x < img->width; ++x)
        {
            if (fread(img->data + i, sizeof(struct pixel), 1, in) != 1)
            {

                image_deinit(img);
                return false;
            }
            ++i;
        }
        if (fseek(in, padding, SEEK_CUR) != 0)
        {
            image_deinit(img);
            return false;
        }
    }
    return true;
}

static struct bmp_header get_header(struct image *img)
{
    uint64_t height = img->height;
    uint64_t width = img->width;
    struct bmp_header header = {
        .bfType = 0x4D42,
        .bfileSize = (sizeof(struct bmp_header) + height * width * sizeof(struct pixel) + height * (4 - ((width * 3 % 4)) % 4)),
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = height * width * sizeof(struct pixel) + (4 - ((width * 3 % 4)) % 4) * height,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0};
    return header;
}

bool to_bmp(FILE *in, struct image *img)
{
    if (!in)
        return false;

    struct bmp_header new_header = get_header(img);
    uint32_t const padding = padding_calculate(img);
    new_header.biSizeImage = img->height * (img->width * sizeof(struct pixel) + (4 - (img->width * 3 % 4)) % 4);

    char a[] = {0, 0, 0};
    if (fwrite(&new_header, sizeof(struct bmp_header), 1, in) != 1)
    {
        return false;
    }
    for (uint32_t i = 0; i < img->height; i++)
    {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, in) != img->width)
        {
            return false;
        }
        if (fwrite(a, 1, padding, in) != padding)
        {
            return false;
        }
    }
    return true;
}
