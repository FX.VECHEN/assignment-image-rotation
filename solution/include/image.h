#ifndef ASSIGNMENT_IMAGE_ROTATION_MASTER_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_MASTER_IMAGE_H

#include <inttypes.h>
#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};
#endif
