
#include "../include/image.h"
#include <mm_malloc.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct image image_set(size_t height, size_t width);
bool from_bmp(FILE *in, struct image *img);
bool to_bmp(FILE* in, struct image * img);
struct image rotate(struct image *image);
void image_deinit(struct image *img);
